/*
	We would like to see the Mysql query to get a simple list of users
   	and their mean score order by mean score desc and group by year.
 */


select   year,
         concat(u.firstname, ' ', u.lastname) as name,
         avg(us.score) as mean
from     users u,
         user_scores us
where    u.id = us.user_id
group by us.year asc,
         u.firstname,
         u.lastname
order by us.year asc,
         mean desc;
