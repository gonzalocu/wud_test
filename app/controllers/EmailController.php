<?php

//use PHPMailer;

class EmailController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function sendWelcomeEmail($name, $email)
	{
        switch(\Config::get('app.email_library')) {
            case 'phpmailer':
                $this->sendByPhpMailer($name, $email);
                break;
            case 'swiftmailer':
                $this->sendBySwiftmailer($name, $email);
                break;
        }
	}

    protected function sendByPhpMailer($name, $email)
    {

        $mail = new PHPMailer;

        //My Settings
        $mail->Host = 'smtp.mailtrap.io';
        $mail->Username = '72ef8af6e09952';
        $mail->Password = '5363a2f3a458dd';
        $mail->Port = 465;

        //Their Settings
        // $mail->Host = \Config::get('mail.host');
        // $mail->Username = \Config::get('mail.username');
        // $mail->Password = \Config::get('mail.password');
        // $mail->Port = \Config::get('mail.port');

        $mail->isSMTP();
        $mail->SMTPAuth = true;
        $mail->SMTPSecure = 'tls';


        $mail->setFrom(\Config::get('app.email_from'), 'wud-techtest');
        $mail->addAddress($email, $name);
        $mail->isHTML(true);

        $mail->Subject = 'Welcome';
        $mail->Body    = 'to WUD';

        $mail->send();

    }

    protected function sendBySwiftmailer($name, $email)
    {
        //My Settings
        $transport = \Swift_SmtpTransport::newInstance('smtp.mailtrap.io', 465)
                    ->setUsername('72ef8af6e09952')
                    ->setPassword('5363a2f3a458dd');

        //Their Settings
        // $transport = \Swift_SmtpTransport::newInstance(\Config::get('mail.host'), \Config::get('mail.port'))
        //             ->setUsername(\Config::get('mail.username'))
        //             ->setPassword(\Config::get('mail.password'));

        $mailer = \Swift_Mailer::newInstance($transport);

        $message = \Swift_Message::newInstance('Welcome')
            ->setFrom(array(\Config::get('app.email_from') => 'wud-techtest'))
            ->setTo(array($email => $name))
            ->setBody('to WUD');

        $mailer->send($message);

    }



}
